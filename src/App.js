import React, {Fragment, useEffect, useState} from 'react';
import {
  Box,
  CircularProgress,
  Container,
  FormControl,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  Typography,
} from "@material-ui/core";
import axios from "axios";
import Search from '@material-ui/icons/Search';
import 'fontsource-roboto';

import './App.css';

const App = () => {
  const [data, setData] = useState({});
  const [img, setImg] = useState('');
  const [loading, setLoading] = useState(true);
  const [defaultCity, setDefaultCity] = useState('Varna');

  const apiRequest = () => {
    axios.get(`https://api.openweathermap.org/data/2.5/weather?q=${defaultCity}&units=metric&lang=bg&appid=a8395b79f121325b8d6dbbc654e18535`)
      .then(response => setData(response.data))
      .then(response => setLoading(false));
  };

  const handleChange = (event) => {
    setDefaultCity(event.target.value);
  };

  const handleSubmit = (event) => {
    apiRequest();
    event.preventDefault();
  };

  useEffect(() => {
    apiRequest();
  }, []);

  useEffect(() => {
    let imgName = data.weather ? data.weather[0].icon : false;
    setImg(`http://openweathermap.org/img/wn/${imgName}@2x.png`);
  }, [data]);

  return (
    <div className="App">
      <Container
        maxWidth="md"
      >
        <Box
          textAlign="center"
        >
          {loading ? <CircularProgress/> :
            <Fragment>
              <form onSubmit={handleSubmit}>
                <FormControl variant="outlined">
                  <InputLabel htmlFor="city">Град</InputLabel>

                  <OutlinedInput
                    id="city"
                    type="text"
                    value={defaultCity}
                    labelWidth={70}
                    onChange={handleChange}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="search"
                          onClick={handleSubmit}
                          edge="end"
                        >
                          <Search/>
                        </IconButton>
                      </InputAdornment>
                    }
                  />
                </FormControl>
              </form>

              <div>
                <img src={img} alt=""/>
              </div>

              <Typography
                variant="h5"
                component="h3"
              >
                {data.weather ? data.weather[0].description : ''}
              </Typography>

              <Typography
                variant="h4"
                component="h2"
              >
                {data.main ? Math.round(data.main.temp) : ''}&deg; - {data.name}, {data.sys ? data.sys.country : ''}
              </Typography>

              <div>
                <small>(Усеща се като: {data.main ? Math.round(data.main.feels_like) : ''}&deg;)</small>
              </div>
            </Fragment>
          }
        </Box>
      </Container>
    </div>
  );
};

export default App;
